#include "chat_box.h"
void *chatterbox_not_found;

ChatterBox *newChatterBox(char *ip_address, char *client_id, int qos, char *topic){
    static int chatterbox_nf;
    int rc;
    if(!chatterbox_not_found) chatterbox_not_found = &chatterbox_nf;
    ChatterBox *chatterbox = malloc(sizeof(ChatterBox));
    *chatterbox = (ChatterBox){.add_new_user=add_user,
			       .remove_existing_user=remove_user,
			       .start = start_chatter_box,
			       .message_received = set_message_listener,
			       .stop = stop_chatter_box,.send_message=send_new_message,
			       .show_friends = display_users,.qos=qos, .client_id=client_id,.friends=NULL,.topic=topic};
    if((rc = MQTTAsync_create(&chatterbox->client, ip_address, client_id, MQTTCLIENT_PERSISTENCE_NONE, NULL))!= MQTTASYNC_SUCCESS){
      
      printf("Instance creation failed \n");
    }
    
    MQTTAsync_setCallbacks(chatterbox->client, chatterbox,on_connection_lost, on_message_arrived,on_delivery);
    
    return chatterbox;
}


void add_user(struct ChatterBox *chatterbox, char * username){
  chatterbox->friends = g_slist_append(chatterbox->friends,username);
  printf("Just added %s as the %d friend \n",username, g_slist_length(chatterbox->friends));
}
void remove_user(struct ChatterBox *chatterbox,  char * username){
  
  chatterbox->friends = g_slist_remove(chatterbox->friends, username);
}
void start_chatter_box(struct ChatterBox *chatterbox){
  MQTTAsync_connectOptions conn_opts = MQTTAsync_connectOptions_initializer;
  
  conn_opts.keepAliveInterval = 20;
  conn_opts.cleansession = 1;
  conn_opts.onSuccess = on_connect;
  conn_opts.onFailure = on_connection_failure;
  conn_opts.context = chatterbox;
  int rc;
  if((rc = MQTTAsync_connect(chatterbox->client, &conn_opts)) != MQTTASYNC_SUCCESS){
    
    printf("Failed to connect, return code %d\n", rc);
        exit(-1);
  }
  
  
}
void stop_chatter_box(struct ChatterBox *chatterbox){
  MQTTAsync_disconnectOptions disc_opts = MQTTAsync_disconnectOptions_initializer;
  disc_opts.onSuccess = on_disconnect;
  disc_opts.context = chatterbox;
  int rc = MQTTAsync_disconnect(chatterbox->client, &disc_opts);
  if (rc != MQTTASYNC_SUCCESS)
	{
		printf("Failed to start disconnect, return code %d\n", rc);
		exit(-1);	
	}
  MQTTAsync_destroy(&chatterbox->client);
  g_slist_free(chatterbox->friends);
}
void display_users(struct ChatterBox* chatterbox, ListFriendsCallback friendlist_callback){
	  friendlist_callback(chatterbox->friends);
}
void on_delivery_failed(void* context, MQTTAsync_failureData* response){
   printf("Delivery failed failed, rc %d\n", response ? response->code : 0);
}
void send_new_message(ChatterBox *chatterbox,char *user_name, char *message){
      MQTTAsync_message msg = MQTTAsync_message_initializer;
      MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
      opts.onFailure = on_delivery_failed;
      opts.context = chatterbox;
      int rc;
      msg.payload = message;
      msg.payloadlen = strlen(message);
      msg.qos = 1;
      msg.retained = 0;
      if ((rc = MQTTAsync_sendMessage(chatterbox->client, user_name, &msg, &opts)) != MQTTASYNC_SUCCESS)
	{
		printf("Failed to start sendMessage, return code %d\n", rc);
 		exit(-1);	
	}
      
}
void set_message_listener(struct ChatterBox* chatterbox,MessageDisplayCallback callback){
  if(callback != NULL){
    chatterbox->msgCallback = callback;
  }
}


/* Internal callbacks for on screen display of data */

void on_connect(void* context, MQTTAsync_successData* response){
      ChatterBox *chatterbox = (ChatterBox*)context;
      MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
      opts.onSuccess = on_subscribe;
      opts.onFailure = on_subscribe_failed;
      opts.context = chatterbox;
      int rc = MQTTAsync_subscribe(chatterbox->client, chatterbox->topic, chatterbox->qos, &opts);
      if (rc != MQTTASYNC_SUCCESS)
	{
		printf("Failed to start subscribe, return code %d\n", rc);
		exit(-1);	
	}
}

void on_connection_failure(void* context, MQTTAsync_failureData* response){
     printf("Connect failed, rc %d\n", response ? response->code : 0);
}
void on_disconnect(void* context, MQTTAsync_successData* response){
     printf("Successful disconnection\n");
}
void on_delivery(void* context, MQTTAsync_token token){
        
	printf("Message with token value %d delivery confirmed\n", token);

}
void on_subscribe(void* context, MQTTAsync_successData* response){
  printf("Subscribe succeeded\n");
}
void on_subscribe_failed(void* context, MQTTAsync_failureData* response){
  printf("Subscribe failed, rc %d\n", response ? response->code : 0);
}
void on_connection_lost(void *context, char *cause){
  ChatterBox *chatterbox = (ChatterBox*)context;
	MQTTAsync_connectOptions conn_opts = MQTTAsync_connectOptions_initializer;
	int rc;

	printf("\nConnection lost\n");
	printf("     cause: %s\n", cause);
	
	printf("Reconnecting\n");
	conn_opts.keepAliveInterval = 20;
	conn_opts.cleansession = 1;
	conn_opts.context = chatterbox;
	if ((rc = MQTTAsync_connect(chatterbox->client, &conn_opts)) != MQTTASYNC_SUCCESS)
	{
		printf("Failed to start connect, return code %d\n", rc);
	    
	}
}
int on_message_arrived(void *context, char *topicName, int topicLen, MQTTAsync_message *message){
      
      
      ChatterBox *chatterbox = (ChatterBox*)context;
      time_t rawtime;
      struct tm * timeinfo;
      time(&rawtime);
      timeinfo = localtime(&rawtime);
      if (chatterbox->msgCallback != NULL){
          chatterbox->msgCallback(topicName,(char*)message->payload, asctime(timeinfo));
      }
      MQTTAsync_freeMessage(&message);
      MQTTAsync_free(topicName);
      return 1;
  
}



