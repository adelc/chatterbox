#ifndef CHATTER_BOX_H
#define CHATTER_BOX_H

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <MQTTAsync.h>
#include <glib.h>
#include <time.h>
typedef void(*ListFriendsCallback)(GSList *);
typedef void(*MessageDisplayCallback)(char *, char *, char *);
typedef struct ChatterBox{
GSList *friends;  
MQTTAsync client;
char *client_id;
char *username;
char *topic;
int qos;
MessageDisplayCallback msgCallback;
void(*add_new_user)(struct ChatterBox*,  char *);  
void(*remove_existing_user)(struct ChatterBox *,  char *);
void(*start)(struct ChatterBox*);
void(*stop)(struct ChatterBox*);
void(*show_friends)(struct ChatterBox*, ListFriendsCallback);
void(*send_message)(struct ChatterBox*, char *, char *);
void(*message_received)(struct ChatterBox*, MessageDisplayCallback);
}ChatterBox;

ChatterBox *newChatterBox(char *ip_address, char *client_id, int qos, char *topic);
void add_user(struct ChatterBox *chatterbox,  char * username);
void remove_user(struct ChatterBox *chatterbox,  char * username);
void start_chatter_box(struct ChatterBox *chatterbox);
void stop_chatter_box(struct ChatterBox *chatterbox);
void display_users(struct ChatterBox* chatterbox, ListFriendsCallback friendlist_callback);
void send_new_message(ChatterBox *chatterbox,char *user_name, char *message);
void set_message_listener(struct ChatterBox* chatterbox,MessageDisplayCallback  callback);
void on_connect(void* context, MQTTAsync_successData* response);
void on_connection_failure(void* context, MQTTAsync_failureData* response);
void on_disconnect(void* context, MQTTAsync_successData* response);
void on_delivery(void* context, MQTTAsync_token token);
void on_delivery_failed(void* context, MQTTAsync_failureData* response);
void on_subscribe(void* context, MQTTAsync_successData* response);
void on_subscribe_failed(void* context, MQTTAsync_failureData* response);
void on_connection_lost(void *context, char *cause);
int on_message_arrived(void *context, char *topicName, int topicLen, MQTTAsync_message *message);



#endif // CHATTER_BOX_H