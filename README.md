**# CHATTER BOX #**
This is a simple wrapper over an mqtt client with the intention of teaching you how to continue abstracting 
stuff in C, and well , build a chat application. 
This will be a relatively easy application for you to build and hopefully cleaner than the previous one
## DEPENDENCIES ##

You will need a linux box that has the base following installed
1. glib
2. PKG CONF
3. git

**### Installing paho-mqtt library for c ###
**
git clone https://git.eclipse.org/r/paho/org.eclipse.paho.mqtt.c
make
sudo make install

## Working On the project ##
To help you build and test , the makefile has a given structure that will show you how the files anre linked together,
and will help you build the code base.
So , all you have to do is run `make build` and to test `make run`

You have the client_ui.h and client_ui.c , I would suggest that you use these files to build up a controller over ChatterBox , You can use your imagination and also get a good grip on making C appear to be an OO language {We all know it is not but, we can make it look really clean }

You are free to read up on any interface you can use but , i think command line is a good start just to get the feel of it.

For example , you can create an instance of the chatterbox client like this

```
#!c
#define ADDRESS     "tcp://m2m.eclipsIDe.org:1883"
#define CLIENTID    "TestID"
#define USERNAME       "ADELINE"
#define QOS         1

ChatterBox *chatter_box = newChatterBox(ADDRESS, CLIENT, QOS);
chatter_box->start(chatter_box);
/* Look at the header chat_box.h to see what else you can call */
chatter_box->stop(chatter_box);

```

### Need help ? ###

You can simply write an email to ask for advice on what to do