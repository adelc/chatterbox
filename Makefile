# The Make file for chatter box

C_SRC = \
chat_code/chat_box.h \
chat_code/chat_box.c \
client_ui.h \
client_ui.c \
main.c

C_LIBS = \
-lpaho-mqtt3as \
-lpaho-mqtt3cs

C_PKG_PATH = \
`pkg-config --cflags --libs glib-2.0`
build:
	gcc $(C_SRC) $(C_LIBS) $(C_PKG_PATH) -o chatter_box
run:
	./chatter_box