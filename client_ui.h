#ifndef CLIENT_H
#define CLIENT_H

#include "chat_code/chat_box.h"
void send_message(char *receipient , char *message);
void on_message_received(char *sender, char *message, char *timestamp);
void show_friends(GSList *friends); 

#endif // CLIENT_H