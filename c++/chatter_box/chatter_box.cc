#include "chatter_box.h"

ChatterBox::ChatterBox(ChatterBoxListener *chatterbox_listener,std::string host, std::string clientId){
  
  this->chatterbox_listener = chatterbox_listener;
  this->client_id = clientId;
  MQTTAsync_create(&this->client, host.c_str(), clientId.c_str(), MQTTCLIENT_PERSISTENCE_NONE, NULL);
  
  MQTTAsync_setCallbacks(this->client, this,ChatterBox::onConnectionLostIn, 
			 ChatterBox::onMessageArrivedIn,ChatterBox::onDeliveryIn);
  
}
ChatterBox::~ChatterBox(){
  
  
}


/*
 * 
 Internal Callbacks to be used by the mqtt client
 
 */
void ChatterBox::startChatterBox(){
  
  MQTTAsync_connectOptions conn_opts = MQTTAsync_connectOptions_initializer;
  
  conn_opts.keepAliveInterval = 10;
  conn_opts.cleansession = 1;
  conn_opts.onSuccess = ChatterBox::onConnectIn;
  conn_opts.onFailure = ChatterBox::onConnectionFailureIn;
  conn_opts.context = this;
  
  int rc;
  if((rc = MQTTAsync_connect(this->client, &conn_opts)) != MQTTASYNC_SUCCESS){
    
    printf("Failed to connect, return code %d\n", rc);
        exit(-1);
  }
  
    
}
 void ChatterBox::stopChatterBox(){
    MQTTAsync_disconnectOptions disc_opts = MQTTAsync_disconnectOptions_initializer;
    disc_opts.onSuccess = ChatterBox::onDisconnectIn;
    disc_opts.context = this;
    int rc = MQTTAsync_disconnect(this->client, &disc_opts);
    if (rc != MQTTASYNC_SUCCESS)
	{
		printf("Failed to start disconnect, return code %d\n", rc);
		exit(-1);	
	}
     MQTTAsync_destroy(&this->client); 
     
 }
void ChatterBox::sendMessageData(std::string topic_name, std::string data){
      MQTTAsync_message msg = MQTTAsync_message_initializer;
      MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
      opts.onFailure = ChatterBox::onDeliveryFailedIn;
      opts.context = this;
      int rc;
      msg.payload = &data[0];
      msg.payloadlen = data.length();
      msg.qos = 1;
      msg.retained = 0;
      if ((rc = MQTTAsync_sendMessage(this->client, topic_name.c_str(), &msg, &opts)) != MQTTASYNC_SUCCESS)
	{
		printf("Failed to start sendMessage, return code %d\n", rc);
 		exit(-1);	
	}
  
}
void ChatterBox::onConnectIn(void* context, MQTTAsync_successData* response){
	    ChatterBox *chatter_box = static_cast<ChatterBox*>(context);
	    chatter_box->chatterbox_listener->onConnect(response);
	    
        MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
        opts.onSuccess = ChatterBox::onSubscribeIn;
        opts.onFailure = ChatterBox::onSubscribeFailedIn;
        opts.context = context;
	int rc = MQTTAsync_subscribe(chatter_box->client, &chatter_box->client_id[0], 0, &opts);
        if (rc != MQTTASYNC_SUCCESS)
	  {
		printf("Failed to start subscribe, return code %d\n", rc);
		exit(-1);	
	  }
        
	        
    
}
void ChatterBox::onConnectionFailureIn(void* context, MQTTAsync_failureData* response){
	    static_cast<ChatterBox*>(context)->chatterbox_listener->onConnectFailed(response);
  
  
}
void ChatterBox::onDisconnectIn(void* context, MQTTAsync_successData* response){
	    static_cast<ChatterBox*>(context)->chatterbox_listener->onDisconnect(response);
  
}
void ChatterBox::onDeliveryIn(void* context, MQTTAsync_token token){
	    static_cast<ChatterBox*>(context)->chatterbox_listener->onMessageDelivered(token);
  
}
void ChatterBox::onDeliveryFailedIn(void* context, MQTTAsync_failureData* response){
	    static_cast<ChatterBox*>(context)->chatterbox_listener->onMessageDeliveryFailed(response);
  
}
void ChatterBox::onSubscribeIn(void* context, MQTTAsync_successData* response){
	    static_cast<ChatterBox*>(context)->chatterbox_listener->onSubscribe(response);
  
}
void ChatterBox::onSubscribeFailedIn(void* context, MQTTAsync_failureData* response){
	    static_cast<ChatterBox*>(context)->chatterbox_listener->onSubscribeFailed(response);
  
}
void ChatterBox::onConnectionLostIn(void *context, char *cause){
	   std::string cause_str(cause);
	   static_cast<ChatterBox*>(context)->chatterbox_listener->onConnectionLost(cause_str);
           MQTTAsync_connectOptions conn_opts = MQTTAsync_connectOptions_initializer;
	   int rc;
    	   printf("\nConnection lost\n");
    	   printf("     cause: %s\n", cause);
    	   printf("Reconnecting\n");
    	   conn_opts.keepAliveInterval = 20;
    	   conn_opts.cleansession = 1;
    	   conn_opts.context = context;
  	   conn_opts.onSuccess = ChatterBox::onConnectIn;
  	   conn_opts.onFailure = ChatterBox::onConnectionFailureIn;
    	   
	   if ((rc = MQTTAsync_connect(static_cast<ChatterBox*>(context)->client, &conn_opts)) != MQTTASYNC_SUCCESS)
    	   {
    		  printf("Failed to start connect, return code %d\n", rc);
    	    
    	   }
}
int ChatterBox::onMessageArrivedIn(void *context, char *topicName, int topicLen, MQTTAsync_message *message){
  	    std::string topic_name = "";
	    if(topicName != NULL){
	       topic_name = std::string(topicName);
	    }
	    static_cast<ChatterBox*>(context)->chatterbox_listener->onMessageArrived(topic_name, topicLen, message); 
	    
}
