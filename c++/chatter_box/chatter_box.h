#ifndef CHATTER_BOX_H
#define CHATTER_BOX_H

#include <MQTTAsync.h>
#include <glib.h>
#include <time.h>
#include "chatterbox_listener.h"
#include <string>
#include <cstdlib>
#include <iostream>

class ChatterBox{
  
public:
  ChatterBox(ChatterBoxListener *chatterbox_listener,std::string host, std::string clientId);
  void startChatterBox();
  void stopChatterBox();
  void sendMessageData(std::string topic_name, std::string data);
  
  virtual ~ChatterBox();
private:
static void onConnectIn(void* context, MQTTAsync_successData* response);
static void onConnectionFailureIn(void* context, MQTTAsync_failureData* response);
static void onDisconnectIn(void* context, MQTTAsync_successData* response);
static void onDeliveryIn(void* context, MQTTAsync_token token);
static void onDeliveryFailedIn(void* context, MQTTAsync_failureData* response);
static void onSubscribeIn(void* context, MQTTAsync_successData* response);
static void onSubscribeFailedIn(void* context, MQTTAsync_failureData* response);
static void onConnectionLostIn(void *context, char *cause);
static int onMessageArrivedIn(void *context, char *topicName, int topicLen, MQTTAsync_message *message);

MQTTAsync client;
std::string client_id;
char *username;
int qos;

ChatterBoxListener *chatterbox_listener;  
};

#endif // CHATTER_BOX_H
