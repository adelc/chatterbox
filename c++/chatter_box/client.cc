#include "client.h"
        ChatterBoxClient::ChatterBoxClient(){
               
               this->window = initscr();
               this->screen = initCDKScreen(this->window);
	       initCDKColor();
	       startClient();
	       menuBox(); 
	       
	       
	       
	 endCDK ();
	       
        }
        
       void ChatterBoxClient::menuBox(){
	 const char *menu_buttons[] = {"Quit", "Friends", "Chat"};
	 this->menu_box = newCDKButtonbox(this->screen,0,
                                                  0,LINES * 0.8,COLS,"Chatter Box Client",1,3,(CDK_CSTRING2) menu_buttons,3, A_REVERSE,
                                                  TRUE, FALSE);
	 bindCDKObject(vBUTTONBOX,this->menu_box,KEY_ENTER,ChatterBoxClient::menu_box_callback,this);
	 drawCDKButtonbox(this->menu_box,TRUE);
	 activateCDKButtonbox(this->menu_box, 0);      
      } 
      ChatterBoxClient::~ChatterBoxClient(){
             
             
             
      }
      void ChatterBoxClient::friendsChoice(){
	    const char *buttons[] = {"Add Friends", "Remove Friends", "Cancel"};
	    const char *message[] = {"<C></U> Friend Options ",
			       "",
			       "<C> The options below are for the addition or removal of friends"};
	    this->friends_dialog = newCDKDialog(this->screen,CENTER, CENTER,(CDK_CSTRING2) message, 3, (CDK_CSTRING2)buttons,3,A_REVERSE,TRUE,TRUE,FALSE);
	    bindCDKObject(vDIALOG,this->friends_dialog,KEY_ENTER,ChatterBoxClient::friend_options_callback,this);
	    drawCDKDialog(this->friends_dialog,TRUE);
	    activateCDKDialog(this->friends_dialog,0);
	    
      }
      int ChatterBoxClient::friend_options_callback(EObjectType objecttype, void *object, void *data, chtype type){
	    CDKDIALOG *dialog = (CDKDIALOG*)object;
	 
	    switch(dialog->currentButton){
	      
	      
	      case ADD_FRIEND:{ destroyCDKDialog(dialog);
		ChatterBoxClient *client = static_cast<ChatterBoxClient*>(data);
		client->addFriends();
		break;
	      }                 
				 
				
	      case REMOVE_FRIEND:{ destroyCDKDialog(dialog);
		ChatterBoxClient *client = static_cast<ChatterBoxClient*>(data);
		client->friendsRemoval();
		break;
	      }
		
		                   
				   
	      case EXIT: { destroyCDKDialog(dialog);} 
				break;
	   		 }
	    
      }
      void ChatterBoxClient::friendsRemoval(){
	char **inits=0;
	this->friend_list = newCDKScroll(this->screen,0,0,CENTER,LINES * 0.8,COLS,"<C></4>Friends Removal", inits,0,TRUE, A_REVERSE,
               TRUE,FALSE);  
	for(std::list<std::string> ::const_iterator it = friends_holder.begin(); it != friends_holder.end(); ++it){
		addCDKScrollItem(this->friend_list,it->c_str());
	  }
	     
	    bindCDKObject(vSCROLL,this->friend_list,KEY_ENTER,ChatterBoxClient::friend_remove_callback,this);
	    drawCDKScroll(this->friend_list, TRUE);
	    activateCDKScroll(this->friend_list,0);
      }
      int ChatterBoxClient::friend_name_callback(EObjectType objecttype GCC_UNUSED, void *object GCC_UNUSED,
                                      void *data, chtype key){
		CDKENTRY *name_entry = (CDKENTRY*)object;
		std::string name(name_entry->info);
		ChatterBoxClient *chatter_box_client = static_cast<ChatterBoxClient*>(data);
		chatter_box_client->friends_holder.push_back(name);
		destroyCDKEntry(name_entry);
		chatter_box_client->menuBox();
      }
    
      int ChatterBoxClient::friend_remove_callback(EObjectType objecttype GCC_UNUSED, void *object GCC_UNUSED,
                                      void *data, chtype key){
	CDKSCROLL *scroll = (CDKSCROLL *)object;
	    int current_index = getCDKScrollCurrent(scroll);
	    ChatterBoxClient *chatter_box_client = static_cast<ChatterBoxClient*>(data);
	    std::list<std::string>::iterator it = std::next(chatter_box_client->friends_holder.begin(),current_index);
	    
	    chatter_box_client->friends_holder.erase(it);
	    destroyCDKScroll(scroll);
	    chatter_box_client->menuBox();
			
      }
    	
    	
      void ChatterBoxClient::chatChoice(){
	   	
	   char **inits = 0;
	   this->friend_list = newCDKScroll(this->screen,CENTER,CENTER,RIGHT,LINES * 0.8,COLS,"<C></5>Friends List", inits,0,TRUE,A_REVERSE,
               TRUE,FALSE);
	   for(std::list<std::string>::const_iterator it = friends_holder.begin(); it != friends_holder.end(); ++it){
		
		addCDKScrollItem(this->friend_list,it->c_str());
		
	   }
	  	
	    
	    bindCDKObject(vSCROLL,this->friend_list,KEY_ENTER,ChatterBoxClient::friend_select_callback,this);
	    drawCDKScroll(this->friend_list, TRUE);
	    activateCDKScroll(this->friend_list,0);
	    if(this->friend_list->exitType == vESCAPE_HIT){
	      destroyCDKScroll(this->friend_list);
	      menuBox();
	    }
	    
	
      }
      int ChatterBoxClient::menu_box_callback(EObjectType objecttype, void *object, void *data, chtype type){
		CDKBUTTONBOX *menu_instance = (CDKBUTTONBOX*)object;
		ChatterBoxClient *client = static_cast<ChatterBoxClient*>(data);
		switch(menu_instance->currentButton){
		  case QUIT:{ destroyCDKButtonbox(client->menu_box);
		              client->stopClient();
		               
		             break;}
		  case FRIENDS:{ 
		              destroyCDKButtonbox(client->menu_box);
		              client->friendsChoice();
			      
			      break;}
		  case CHAT:{ 
			      destroyCDKButtonbox(client->menu_box);
		              client->chatChoice(); 
			      
			      break;}
		}
		return (TRUE);
      }
      void ChatterBoxClient::cleanWidgets(){
	
	
      }
      
        
      void ChatterBoxClient::startClient(){
            
            this->chatterBox = new ChatterBox(this,"tcp://iot.eclipse.org:1883","0791149189");
	    this->chatterBox->startChatterBox();
	   
	    
      }
      
     
      int ChatterBoxClient::friend_select_callback(EObjectType objecttype, void *object, void *data, chtype type){
	    CDKSCROLL *scroll = (CDKSCROLL *)object;
	    int current_index = getCDKScrollCurrent(scroll);
	    ChatterBoxClient *chatter_box_client = static_cast<ChatterBoxClient*>(data);
	    std::list<std::string>::iterator it = std::next(chatter_box_client->friends_holder.begin(),current_index);
	    
	    chatter_box_client->current_friend_code = (*it);
	    destroyCDKScroll(scroll);
	    chatter_box_client->messages_list = newCDKSwindow(chatter_box_client->screen,0,0,LINES*0.8,
                                                   COLS,"<C></5> Messages ",
                                                   100,TRUE,FALSE);
               
            chatter_box_client->message_entry = newCDKMentry(chatter_box_client->screen,0,
							     getbegy(chatter_box_client->messages_list->win)+chatter_box_client->messages_list->boxHeight+1,
                                                  "<C></5>Enter a message.<!5>","",
                                                  A_BOLD,'.', vMIXED,chatter_box_client->messages_list->boxWidth-1,
                                                  2, 4,
                                                  0,TRUE,FALSE);
	    
	    bindCDKObject(vMENTRY,chatter_box_client->message_entry,KEY_ENTER, ChatterBoxClient::entry_callback, chatter_box_client);
	    drawCDKSwindow (chatter_box_client->messages_list, ObjOf (chatter_box_client->messages_list)->box);
	    
	    do{
	        activateCDKMentry(chatter_box_client->message_entry,0);
	      
	    }while(chatter_box_client->message_entry->exitType != vESCAPE_HIT);
	     destroyCDKMentry(chatter_box_client->message_entry);
	     destroyCDKSwindow(chatter_box_client->messages_list);
	     static_cast<ChatterBoxClient*>(data)->chatChoice();
      }
      
      
      int ChatterBoxClient::entry_callback(EObjectType objecttype GCC_UNUSED, void *object GCC_UNUSED,
                                  void *data, chtype key){
           CDKMENTRY *entry = (CDKMENTRY *)object;
           std::string message(entry->info);
	   std::string route_code = static_cast<ChatterBoxClient*>(data)->current_friend_code;
	   ChatterBoxClient *client = static_cast<ChatterBoxClient*>(data);
           client->sendMessage(route_code,message);
	   std::string msg("<L></4>");
	   time_t rawtime;
           struct tm * timeinfo;
           time(&rawtime);
           timeinfo = localtime(&rawtime);
	   std::string display_time(asctime(timeinfo)); // The Time to be displayed
	   msg += display_time;
	   msg += ":";
	   msg += entry->info;
	   addCDKSwindow (client->messages_list,msg.c_str(), BOTTOM);
           cleanCDKMentry(entry);
	   drawCDKMentry(client->message_entry,TRUE);
                 return (TRUE);                    
            }
      
      void ChatterBoxClient::addFriends(){
         this->friend_code_entry = newCDKEntry (this->screen,CENTER,CENTER,
                            "<C>Enter a\n<C>friends name.",  "</U/5>Name:<!U!5>", A_NORMAL, '.', vMIXED,
                            40, 0, 256,
                            TRUE,
                            FALSE);
	 bindCDKObject (vENTRY, this->friend_code_entry,KEY_ENTER, ChatterBoxClient::friend_entry_callback, this);
	 drawCDKEntry(this->friend_code_entry,TRUE);
	 activateCDKEntry (this->friend_code_entry, 0);

      }
      int ChatterBoxClient::friend_entry_callback(EObjectType objecttype GCC_UNUSED, void *object GCC_UNUSED,
                                  void *data, chtype key){
	CDKENTRY *entry = (CDKENTRY *)object;
	std::string name(entry->info);
	static_cast<ChatterBoxClient*>(data)->friends_holder.push_back(name);  
	destroyCDKEntry(entry);
	refreshCDKScreen(static_cast<ChatterBoxClient*>(data)->screen);
	static_cast<ChatterBoxClient*>(data)->menuBox();
	
      }
      void ChatterBoxClient::exitProgram(){
	 destroyCDKScreen(this->screen);
         endCDK ();
         exit(0);
      }
	   void ChatterBoxClient::stopClient(){

	   chatterBox->stopChatterBox();
           destroyCDKScreen(this->screen); 
	   endCDK ();
	   exit(0);
            
        }	
	   void ChatterBoxClient::onConnect(MQTTAsync_successData *successData){
            
	    
        }
        void ChatterBoxClient::onConnectFailed(MQTTAsync_failureData *failureData){
   	         
            
        }
        void ChatterBoxClient::onConnectionLost(std::string cause){
            
            
        }
        void ChatterBoxClient::onDisconnect(MQTTAsync_successData *successData){
            
            
        }
        void ChatterBoxClient::onMessageArrived(std::string topic, size_t messageLength, MQTTAsync_message *message){
                  time_t rawtime;
                  struct tm * timeinfo;
                  time(&rawtime);
                  timeinfo = localtime(&rawtime);
                       
                       std::string display_time(asctime(timeinfo)); // The Time to be displayed
		       if((char*)message->payload != NULL){
				std::string msg((char*)message->payload);
				std::string output = "<L></11>";
	   		        output += display_time;
		                output +=":";
		                output +=msg;
		                addCDKSwindow (this->messages_list, output.c_str(), BOTTOM);
		                MQTTAsync_freeMessage(&message);				       
			}
        }
        int ChatterBoxClient::failure_dialog_callback(EObjectType objecttype GCC_UNUSED, void *object GCC_UNUSED,
                                  void *data, chtype key){
	  static_cast<ChatterBoxClient*>(data)->exitProgram();
			
	  
        }
        void ChatterBoxClient::onMessageDelivered(MQTTAsync_token token){
            addCDKSwindow (this->messages_list,"<C></11>Delivered:", BOTTOM);
            
        }
        void ChatterBoxClient::onMessageDeliveryFailed(MQTTAsync_failureData *failureData){
            addCDKSwindow (this->messages_list,"<C></11>Delivery failed:", BOTTOM);
            
        }
        void ChatterBoxClient::onSubscribe(MQTTAsync_successData *successData){
            
            
            
        }
        void ChatterBoxClient::onSubscribeFailed(MQTTAsync_failureData *failureData){
            
            
            
        }
        void ChatterBoxClient::sendMessage(std::string topic,std::string message){
	    this->chatterBox->sendMessageData(topic, message);  
	}
        
        
