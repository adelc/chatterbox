#ifndef CHATTERBOX_LISTENER_H
#define CHATTERBOX_LISTENER_H
#include <string>
#include <MQTTAsync.h>
class ChatterBoxListener{
public:
  virtual void onConnect(MQTTAsync_successData*)=0;
  virtual void onConnectFailed(MQTTAsync_failureData*)=0;
  virtual void onConnectionLost(std::string)=0;
  virtual void onDisconnect(MQTTAsync_successData*)=0;
  virtual void onMessageArrived(std::string, size_t, MQTTAsync_message*)=0;
  virtual void onMessageDelivered(MQTTAsync_token)=0;
  virtual void onMessageDeliveryFailed(MQTTAsync_failureData*)=0;
  virtual void onSubscribe(MQTTAsync_successData*)=0;
  virtual void onSubscribeFailed(MQTTAsync_failureData*)=0;
};

#endif // CHATTERBOX_LISTENER_H