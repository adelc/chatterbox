#ifndef CLIENT_H
#define CLIENT_H
#include "chatter_box.h"
#include <list>
#include <cdk.h>
#include <iterator>



#define ADD_FRIEND 0
#define REMOVE_FRIEND 1
#define EXIT 2


#define QUIT 0
#define FRIENDS 1
#define CHAT 2


class ChatterBoxClient:public ChatterBoxListener{
	public:
        ChatterBoxClient();
        virtual ~ChatterBoxClient();
		void startClient();
		void stopClient();	
		void cleanWidgets();
		void onConnect(MQTTAsync_successData *successData);
        void menuBox();
	void friendsChoice();
	void chatChoice();
        void onConnectFailed(MQTTAsync_failureData *failureData);
        void onConnectionLost(std::string cause);
        void onDisconnect(MQTTAsync_successData *successData);
        void onMessageArrived(std::string topic, size_t messageLength, MQTTAsync_message *message);
        void onMessageDelivered(MQTTAsync_token token);
        void onMessageDeliveryFailed(MQTTAsync_failureData *failureData);
        void onSubscribe(MQTTAsync_successData *successData);
        void onSubscribeFailed(MQTTAsync_failureData *failureData);
        void sendMessage(std::string topic,std::string message);
        void clearMessages();
        void addFriends();
        void exitProgram();
	void friendsRemoval();
	
        static int menu_box_callback(EObjectType objecttype, void *object, void *data, chtype type);
        static int entry_callback(EObjectType objecttype GCC_UNUSED, void *object GCC_UNUSED,
                                  void *data, chtype key);
    	static int friend_entry_callback(EObjectType objecttype GCC_UNUSED, void *object GCC_UNUSED,
                                      void *data, chtype key);
    	static int friend_select_callback(EObjectType objecttype, void *object, void *data, chtype type);
	static int friend_options_callback(EObjectType objecttype, void *object, void *data, chtype type);
    	static int failure_dialog_callback(EObjectType objecttype GCC_UNUSED, void *object GCC_UNUSED,
                                      void *data, chtype key);
	static int friend_name_callback(EObjectType objecttype GCC_UNUSED, void *object GCC_UNUSED,
                                      void *data, chtype key);
    	
        static int friend_remove_callback(EObjectType objecttype GCC_UNUSED, void *object GCC_UNUSED,
                                      void *data, chtype key);
    	
    
        
    private:
	ChatterBox *chatterBox;
        CDKSWINDOW *messages_list;
        CDKSCROLL *friend_list;
        CDKSCREEN *screen;
        CDKMENTRY *message_entry;
	CDKENTRY *friend_code_entry;
        CDKBUTTONBOX *button_box;
        CDKBUTTONBOX *menu_box;
	CDKDIALOG *friends_dialog;
	CDKENTRY *friend_name_entry;
        WINDOW *window;
        int main_pos_x;
        int main_pos_y;
        
    protected:
        void on_friend_search();
        void on_connect();
	std::string current_friend;
	std::string current_friend_code;
	std::list<std::string> friends_holder;
};
#endif // CLIENT_H
